package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import com.epam.rd.java.basic.task7.db.entity.*;

public class DBManager {

    private static DBManager instance;
    private static String URL;

    public static synchronized DBManager getInstance() {
        //try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {

        FileInputStream fis;
        Properties property = new Properties();
        try {
            fis = new FileInputStream("app.properties");
            property.load(fis);
            URL = property.getProperty("connection.url");
            System.out.println("~~~~~~~~~~~~~~~~~~ "+URL);
        } catch (IOException e) {
        }
        //URL="jdbc:mysql://localhost:3336/task07?user=root&password=12345678";
        instance = new DBManager();
        try (Connection con = DriverManager.getConnection(URL)) {
            System.out.println("Ok connected. " + con);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return instance;
    }

    private DBManager() {
    }

    public List<User> findAllUsers() throws DBException {
        System.out.println("~~~~~~~~~~~~~~~<findAllUsers>");
        List<User> listUsers = new ArrayList<>();
        ResultSet result;
        try (Connection con = DriverManager.getConnection(URL)) {
            Statement stmt = con.createStatement();
            result = stmt.executeQuery("SELECT * FROM users");
            while (result.next()) {
                User u = new User();
                u.setId(result.getInt("id"));
                u.setLogin(result.getString("login"));
                listUsers.add(u);
            }
            System.out.println("Ok selected * from table user " + con);
            listUsers.forEach(System.out::println);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("~~~~~~~~~~~~~~~</findAllUsers>");
        return listUsers;
    }

    public boolean insertUser(User user) throws DBException {
        try (Connection con = DriverManager.getConnection(URL)) {
            Statement stmt = con.createStatement();
            stmt.executeUpdate("INSERT INTO users VALUES (DEFAULT,'" + user.getLogin() + "')");
            user.setId(getUser(user.getLogin()).getId());
            System.out.println("Ok inserted USER " + user.getLogin()+", id "+user.getId());
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean deleteUsers(User... users) throws DBException {
        System.out.println("~~~~~~~~~~~~~~~<deleteUsers> ");
        try (Connection con = DriverManager.getConnection(URL)) {
            Statement stmt = con.createStatement();
            for (User u : users) {
                stmt.executeUpdate("DELETE FROM users WHERE id=" + u.getId());
                stmt.executeUpdate("DELETE FROM users_teams WHERE user_id=" + u.getId());
                System.out.println("Ok DELETE from users " + u);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public User getUser(String login) throws DBException {
        System.out.println("~~~~~~~~~~~~~~~<getUser>");
        User user = new User();
        ResultSet result;
        try (Connection con = DriverManager.getConnection(URL)) {
            Statement stmt = con.createStatement();

            result = stmt.executeQuery("SELECT * FROM users WHERE login='" + login + "'");
            while (result.next()) {
                user.setId(result.getInt("id"));
                user.setLogin(result.getString("login"));
            }

            System.out.println("Ok selected * from table user " + login);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("~~~~~~~~~~~~~~~</getUser>");
        return user;
    }

    public Team getTeam(String name) throws DBException {
        System.out.println("~~~~~~~~~~~~~~~<getTeam>");
        Team team = new Team();
        ResultSet result;
        try (Connection con = DriverManager.getConnection(URL)) {
            Statement stmt = con.createStatement();

            result = stmt.executeQuery("SELECT * FROM teams WHERE name='" + name + "'");
            while (result.next()) {
                team.setId(result.getInt("id"));
                team.setName(result.getString("name"));
            }

            System.out.println("Ok selected * from table TEAMS " + name);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("~~~~~~~~~~~~~~~</getTeam>");
        return team;
    }

    public List<Team> findAllTeams() throws DBException {
        System.out.println("~~~~~~~~~~~~~~~<findAllTeams>");
        List<Team> listTeams = new ArrayList<>();
        ResultSet result;
        //listTeams=null;
        try (Connection con = DriverManager.getConnection(URL)) {
            Statement stmt = con.createStatement();
            result = stmt.executeQuery("SELECT * FROM teams");
            while (result.next()) {
                Team t = new Team();
                t.setId(result.getInt("id"));
                t.setName(result.getString("name"));
                listTeams.add(t);
            }
            System.out.println("Ok selected * from table teams " + con);
            listTeams.forEach(System.out::println);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("~~~~~~~~~~~~~~~</findAllTeams>");
        return listTeams;
    }

    public boolean insertTeam(Team team) throws DBException {
        try (Connection con = DriverManager.getConnection(URL)) {
            Statement stmt = con.createStatement();
            stmt.executeUpdate("INSERT INTO teams VALUES (DEFAULT,'" + team.getName() + "')");
            team.setId(getTeam(team.getName()).getId());
            System.out.println("Ok inserted TEAM " + team.getName() + ". " + con);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        try (Connection con = DriverManager.getConnection(URL)) {
            Statement stmt = con.createStatement();
            String sql_str="INSERT INTO users_teams VALUES ";
            for (Team elTeam : teams) {
                        sql_str+= "("+getUser(user.getLogin()).getId() + "," +  getTeam(elTeam.getName()).getId()+"),";
            }
            sql_str=sql_str.substring(0,sql_str.length()-1);
            stmt.executeUpdate(sql_str);
//            for (Team elTeam : teams) {
//                stmt.executeUpdate("INSERT INTO users_teams VALUES ("
//                        + getUser(user.getLogin()).getId() + "," +  getTeam(elTeam.getName()).getId()+ ")");
//            }

        } catch (SQLException e) {
            System.out.println("error *********+++++++++++++++++++++++++++");
            e.printStackTrace();
            throw new DBException("Thrown exception musb be a DBException",e);

        }
        return true;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        ResultSet result;
        List<Team> listTeams = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(URL)) {
            Statement stmt = con.createStatement();
            result = stmt.executeQuery("SELECT id, name FROM teams left JOIN users_teams on id=team_id where user_id=" + getUser(user.getLogin()).getId() );
            while (result.next()) {
                Team t = new Team();
                t.setId(result.getInt("id"));
                t.setName(result.getString("name"));
                listTeams.add(t);
            }
            System.out.println("Ok getUserTeams for user " + user.getLogin());
            listTeams.forEach(System.out::println);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println("user=" + user.getLogin() + " in team=" + listTeams);
        return listTeams;
    }

    public boolean deleteTeam(Team team) throws DBException {
        System.out.println("~~~~~~~~~~~~~~~<deleteTeam> " + team);
        try (Connection con = DriverManager.getConnection(URL)) {
            Statement stmt = con.createStatement();
            stmt.executeUpdate("DELETE FROM teams WHERE id=" + team.getId());
            stmt.executeUpdate("DELETE FROM users_teams WHERE team_id=" + team.getId());
            System.out.println("Ok DELETE from  TEAMS " + team);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        System.out.println("~~~~~~~~~~~~~~~</deleteTeam>");
        return true;
    }

    public boolean updateTeam(Team team) throws DBException {
        System.out.println("~~~~~~~~~~~~~~~<updateTeam> " + team);
        try (Connection con = DriverManager.getConnection(URL)) {
            Statement stmt = con.createStatement();
            stmt.executeUpdate("UPDATE teams SET name='" + team.getName() + "' WHERE id=" + team.getId());
            System.out.println("Ok Update in TEAMS " + team);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        System.out.println("~~~~~~~~~~~~~~~</updateTeam>");
        return true;
    }

}
